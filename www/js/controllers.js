angular.module('starter.controllers', [])

.controller('QuestionsCtrl', function($scope, $ionicSideMenuDelegate, $ionicPopover, $stateParams, $state) {

  if($stateParams.filter == undefined) {
    $stateParams.filter = false;
  }

  console.log($state.current);

  $scope.filter = function() {
    //if ($stateParams) {}
    console.log('filter');
    console.log($stateParams.filter);
    return $stateParams.filter;
  };

  $scope.toggleLeft = function() {
    $ionicSideMenuDelegate.toggleLeft();
  };

  $scope.openPopover = function($event) {
    $ionicPopover.fromTemplateUrl('templates/popover.html', {
      scope: $scope,
    }).then(function(popover) {
      $scope.popover = popover;
      $scope.popover.show($event);
    });
  };

    // moveProgressBar();
    // // on browser resize...
    // $(window).resize(function() {
    //     moveProgressBar();
    // });

    // // SIGNATURE PROGRESS
    // function moveProgressBar() {
    //   console.log("moveProgressBar");
    //     var getPercent = ($('.progress-wrap').data('progress-percent') / 100);
    //     var getProgressWrapWidth = $('.progress-wrap').width();
    //     var progressTotal = getPercent * getProgressWrapWidth;
    //     var animationLength = 2500;

    //     // on page load, animate percentage bar to data percentage length
    //     // .stop() used to prevent animation queueing
    //     $('.progress-bar').stop().animate({
    //         left: progressTotal
    //     }, animationLength);
    // }

})

.controller('ConfigCtrl', function($scope, $ionicHistory) {

  $scope.settings = {
    enablePushNotifications: true
  };

})


.controller('AccountCtrl', function($scope, $ionicPopover) {

  $scope.openPopover = function($event) {
    $ionicPopover.fromTemplateUrl('templates/popover.html', {
      scope: $scope,
    }).then(function(popover) {
      $scope.popover = popover;
      $scope.popover.show($event);
    });
  };

})

.controller('QuestionCtrl', function($scope, $ionicModal) {

 $ionicModal.fromTemplateUrl('templates/questions-info-modal.html', function($ionicModal) {
    $scope.modal = $ionicModal;

  }, {
    // Use our scope for the scope of the modal to keep it simple
    scope: $scope,
    // The animation we want to use for the modal entrance
    animation: 'slide-in-up'
  });


})

.controller('QuestionMultipleCtrl', function($scope, $ionicModal, $document) {

 $ionicModal.fromTemplateUrl('templates/questions-info-modal.html', function($ionicModal) {
    $scope.modal = $ionicModal;

  }, {
    // Use our scope for the scope of the modal to keep it simple
    scope: $scope,
    // The animation we want to use for the modal entrance
    animation: 'slide-in-up'
  });

   $scope.countChanged = function(index) {
    var count = 3;
    var increment = $document[0].getElementsByClassName('increment');
    increment[0].style.width = (1+19*index/(count-1))*5+'%';
  };

  $scope.countChanged(1);

})

.controller('QuestionsMapCtrl', function($scope, $ionicLoading, $ionicModal) {

  $scope.init = function() {
    //var myLatlng = new google.maps.LatLng(37.3000, -120.4833);

    var map = new google.maps.Map(document.getElementById('map'), {
    zoom: 15,
    center: {lat: -23.5545063, lng: -46.6364818}
    });

    var ctaLayer = new google.maps.KmlLayer({
    url: 'https://www.google.com/maps/d/kml?mid=zCO7w_UgS5Jk.kJdHGD118l0A',
    map: map
    });

    $scope.map = map;
  };

  google.maps.event.addDomListener(window, 'load', $scope.init);

   $ionicModal.fromTemplateUrl('templates/questions-info-modal.html', function($ionicModal) {
    $scope.modal = $ionicModal;

  }, {
    // Use our scope for the scope of the modal to keep it simple
    scope: $scope,
    // The animation we want to use for the modal entrance
    animation: 'slide-in-up'
  });

})


.controller('QuestionsSummaryCtrl', function($scope, $ionicLoading, $ionicModal, $state) {

   $ionicModal.fromTemplateUrl('templates/questions-info-modal.html', function($ionicModal) {
    $scope.modal = $ionicModal;

  }, {
    // Use our scope for the scope of the modal to keep it simple
    scope: $scope,
    // The animation we want to use for the modal entrance
    animation: 'slide-in-up'
  });

  $scope.sendAnswers = function() {
    $state.go('questions-finish');
//$ionicLoading.show({ template: 'Pesquisa enviada com sucesso!', noBackdrop: true, duration: 1000 });
  };

});

