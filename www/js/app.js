// Ionic Starter App

// angular.module is a global place for creating, registering and retrieving Angular modules
// 'starter' is the name of this angular module example (also set in a <body> attribute in index.html)
// the 2nd parameter is an array of 'requires'
// 'starter.services' is found in services.js
// 'starter.controllers' is found in controllers.js
angular.module('starter', ['ionic', 'starter.controllers', 'starter.services', 'angular-svg-round-progress','ngCordova'])

.run(function($ionicPlatform, $cordovaStatusbar) {
  $ionicPlatform.ready(function() {
    // Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
    // for form inputs)
    if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
      cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
      cordova.plugins.Keyboard.disableScroll(true);

    }
    if (window.StatusBar) {
      // org.apache.cordova.statusbar required
      StatusBar.styleDefault();
      $cordovaStatusbar.overlaysWebView(true);
      $cordovaStatusbar.styleHex('#820F14');
    }

  });
})

.config(function($stateProvider, $urlRouterProvider, $ionicConfigProvider) {

  $ionicConfigProvider.tabs.position('bottom'); // other values: top


  // Ionic uses AngularUI Router which uses the concept of states
  // Learn more here: https://github.com/angular-ui/ui-router
  // Set up the various states which the app can be in.
  // Each state's controller can be found in controllers.js
  $stateProvider


  .state('welcome', {
    url: '/welcome',
    templateUrl: 'templates/welcome.html'
  })

  .state('login', {
    url: '/login',
    templateUrl: 'templates/login.html'
  })

  .state('config', {
    url: '/config',
    templateUrl: 'templates/config.html',
    controller: 'ConfigCtrl'
  })

  .state('icons', {
    url: '/icons',
    templateUrl: 'templates/icons.html'
  })

  // setup an abstract state for the tabs directive
  .state('tab', {
    url: '/tab',
    abstract: true,
    templateUrl: 'templates/tabs.html'
  })

  // Each tab has its own nav history stack:

  .state('tab.questions', {
    url: '/questions',
    views: {
      'tab-questions': {
        templateUrl: 'templates/tab-questions-sidemenu.html',
        controller: 'QuestionsCtrl'
      }
    }
  })

  .state('tab.questions-filtered', {
    url: '/questions-filtered',
    views: {
      'tab-questions': {
        templateUrl: 'templates/tab-questions-sidemenu-filtered.html',
        controller: 'QuestionsCtrl'
      }
    }
  })

  .state('questions-paragraph', {
    url: '/questions/paragraph',
    templateUrl: 'templates/questions-paragraph.html',
    controller: 'QuestionCtrl'
  })

  .state('questions-map', {
    url: '/questions/map',
    templateUrl: 'templates/questions-map.html',
    controller: 'QuestionsMapCtrl'
  })

  .state('questions-multiple', {
    url: '/questions/multiple',
    templateUrl: 'templates/questions-multiple.html',
    controller: 'QuestionMultipleCtrl'
  })

  .state('questions-multiple-single', {
    url: '/questions/multiple-single',
    templateUrl: 'templates/questions-multiple-single.html',
    controller: 'QuestionCtrl'
  })


  .state('questions-summary', {
    url: '/questions/summary',
    templateUrl: 'templates/questions-summary.html',
    controller: 'QuestionsSummaryCtrl'
  })

  .state('questions-summary-2', {
    url: '/questions/summary',
    templateUrl: 'templates/questions-summary-2.html',
    controller: 'QuestionsSummaryCtrl'
  })

  .state('questions-finish', {
    url: '/questions/finish',
    templateUrl: 'templates/questions-finish.html'
  })

  .state('tab.account', {
    url: '/account',
    views: {
      'tab-account': {
        templateUrl: 'templates/tab-account.html',
        controller: 'AccountCtrl'
      }
    }
  })

  .state('tab.ranking', {
    url: '/account/ranking',
    views: {
      'tab-account': {
        templateUrl: 'templates/ranking.html',
        controller: 'AccountCtrl'
      }
    }
  });

  // if none of the above states are matched, use this as the fallback
  $urlRouterProvider.otherwise('/welcome');

})

.directive('positionBarsAndContent', function($timeout) {

 return {

    restrict: 'AC',

    link: function(scope, element) {

      var offsetTop = 0;

      // Get the parent node of the ion-content
      var parent = angular.element(element[0].parentNode);

      // Get all the headers in this parent
      var headers = parent[0].getElementsByClassName('bar');

      // Iterate through all the headers
      for(x=0;x<headers.length;x++)
      {
        // If this is not the main header or nav-bar, adjust its position to be below the previous header
        if(x > 0) {
          headers[x].style.top = offsetTop + 'px';
        }

        // Add up the heights of all the header bars
        offsetTop = offsetTop + headers[x].offsetHeight;
      }

      // Position the ion-content element directly below all the headers
      element[0].style.top = offsetTop + 'px';

    }
  };
});
